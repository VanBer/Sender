#![windows_subsystem = "windows"]

use eframe::{egui, IconData};
use egui::{Label, FontId, Color32, RichText, TextBuffer, Vec2};
use egui::accesskit::TextSelection;
use winapi::um::winuser::*;
use enigo::*;
use regex::Regex;
use core::time;
use std::fs::File;
use std::io::{Write, Read};
use std::io::{BufReader, BufRead};
use std::thread::sleep;
use encoding_rs::WINDOWS_1251;
use encoding_rs_io::DecodeReaderBytesBuilder;

const APP_NAME: &str = "Sender (beta)";
const DEFAULT_SIZE_WINDOW_X: f32 = 475.0;
const DEFAULT_SIZE_WINDOW_Y: f32 = 420.0;
const DEFAULT_POSITION_WINDOW_X: f32 = 700.0;
const DEFAULT_POSITION_WINDOW_Y: f32 = 300.0;
const INPUT_HEIGHT: f32 = 460.0;
const INPUT_WIDTH: f32 = 20.0;
const BUTTON_HEIGHT: f32 = 460.0;
const BUTTON_GENERAL_WIDTH: f32 = 30.0;
const BUTTON_SMALL_WIDTH: f32 = 20.0;

fn main() {
    let window_options = eframe::NativeOptions {
        initial_window_size: Some(egui::vec2(DEFAULT_SIZE_WINDOW_X, DEFAULT_SIZE_WINDOW_Y)),
        min_window_size: Some(egui::vec2(DEFAULT_SIZE_WINDOW_X, DEFAULT_SIZE_WINDOW_Y)),
        initial_window_pos: Some(egui::pos2(DEFAULT_POSITION_WINDOW_X, DEFAULT_POSITION_WINDOW_Y)),
        resizable: false,
        ..Default::default()
    };

    eframe::run_native(
        APP_NAME, 
        window_options, 
        Box::new(|_cc| Box::new(Sender::new())),
    );
}

struct Sender {
    paths: Paths,
    commands: SampCommands,
    player: Player,
    filter: Filter,
    players_collector: PlayersCollector,
    numbers_collector: NumbersCollector,
    message: Message,
    delay: Delay,
    language: LangSwitcher,
    file: MyFile,
    regex_templates: RegexTemplates
}

impl Sender {
    fn new() -> Self {
        Self {
            paths: Paths::new(),
            commands: SampCommands,
            player: Player::new(),
            filter: Filter,
            players_collector: PlayersCollector::new(),
            numbers_collector: NumbersCollector::new(),
            message: Message::new(),
            delay: Delay,
            language: LangSwitcher,
            file: MyFile,
            regex_templates: RegexTemplates::new()
        }
    }
}

impl eframe::App for Sender {
    fn update(&mut self, ctx: &egui::Context, frame: &mut eframe::Frame) {
        egui::CentralPanel::default().show(ctx, |ui| {
            let title = ui.heading("Панель управления");
            ui.separator();

            let title_chatlog_path = ui.label("Введите имя пользователя в абсолютный путь до файла chatlog.txt.");
            let input_chatlog_path = egui::widgets::TextEdit::singleline(&mut self.paths.chatlog);
            ui.add_sized([INPUT_HEIGHT, INPUT_WIDTH], input_chatlog_path);

            let title_players_path = ui.label("Введите имя пользователя в абсолютный путь до файла players.txt.");
            let input_players_path = egui::widgets::TextEdit::singleline(&mut self.paths.players);
            ui.add_sized([INPUT_HEIGHT, INPUT_WIDTH], input_players_path);

            let title_numbers_path = ui.label("Введите имя пользователя в абсолютный путь до файла numbers.txt.");
            let input_numbers_path = egui::widgets::TextEdit::singleline(&mut self.paths.numbers);
            ui.add_sized([INPUT_HEIGHT, INPUT_WIDTH], input_numbers_path);

            let button_save_paths = egui::Button::new("Сохранить");
            if ui.add_sized([BUTTON_HEIGHT, BUTTON_SMALL_WIDTH], button_save_paths).clicked() {
                let mut paths_list = String::new();
                paths_list.push_str(&self.paths.chatlog);
                paths_list.push_str("\n");
                paths_list.push_str(&self.paths.players);
                paths_list.push_str("\n");
                paths_list.push_str(&self.paths.numbers);
                MyFile::write(self.paths.settings.as_str(), paths_list.as_str());
            }

            let button_recover_paths = egui::Button::new("Восстановить");
            if ui.add_sized([BUTTON_HEIGHT, BUTTON_SMALL_WIDTH], button_recover_paths).clicked() {
                self.paths.chatlog.clear();
                self.paths.players.clear();
                self.paths.numbers.clear();
                let mut paths = MyFile::read(self.paths.settings.as_str());
                let mut chatlog: bool = true;
                let mut players: bool = false;
                let mut numbers: bool = false;
                for path in paths.lines() {
                    if numbers {
                        self.paths.numbers = path.to_string();
                        numbers = false;
                        break;
                    }
                    if players {
                        self.paths.players = path.to_string();
                        players = false;
                        numbers = true;
                        continue;
                    }
                    if chatlog {
                        self.paths.chatlog = path.to_string();
                        chatlog = false;
                        players = true;
                        continue;
                    }

                }
            }
                
            let title_players_count = ui.label("Введите количество игроков, которое нужно перебрать.");
            let input_players_count = egui::widgets::TextEdit::singleline(&mut self.players_collector.count);
            ui.add_sized([38.0, INPUT_WIDTH], input_players_count);

            let title_text_for_sms = ui.label("Введите текст для рассылки.");
            let input_text_for_sms = egui::widgets::TextEdit::singleline(&mut self.message.text);
            ui.add_sized([INPUT_HEIGHT, INPUT_WIDTH], input_text_for_sms);

            let button_collect_players = egui::Button::new("Сбор ID");
            if ui.add_sized([BUTTON_HEIGHT, BUTTON_GENERAL_WIDTH], button_collect_players).clicked() {
                Delay::ten_seconds();
                loop {
                    Delay::one_second();
                    if self.player.id < self.players_collector.count.parse().unwrap() {
                        self.players_collector.print_player(self.player.id);
                        self.players_collector.collect_player(self.paths.chatlog.as_str());
                        self.player.id += 1;
                    }
                    else {
                        self.players_collector.save_players_in_file(&self.paths.players, self.players_collector.list.clone());
                        println!("Collect {} ID-s", self.player.id);
                        break;
                    }
                }
            }
            
            let button_collect_numbers = egui::Button::new("Пробив номеров");
            if ui.add_sized([BUTTON_HEIGHT, BUTTON_GENERAL_WIDTH], button_collect_numbers).clicked() {
                Delay::ten_seconds();
                let id_list = self.players_collector.get_players_from_file(self.paths.players.as_str(), self.players_collector.count.parse().unwrap());
                let mut numbers_list = String::new();
                for id in id_list.iter() {
                    Delay::one_second();
                    SampCommands::print_number(id.parse::<usize>().unwrap());
                    let number = self.numbers_collector.get_number(self.paths.chatlog.as_str());
                    if number != " " {
                        numbers_list.push_str(number.as_str());
                        numbers_list.push_str("\n");
                        self.numbers_collector.counter += 1;
                    }
                    
                }
                MyFile::write(self.paths.numbers.as_str(), numbers_list.as_str());
            }

            let button_send = egui::Button::new("Начать рассылку");
                if ui.add_sized([BUTTON_HEIGHT, BUTTON_GENERAL_WIDTH], button_send).clicked() {
                    Delay::ten_seconds();
                    let sender = Message::new();
                    sender.attack_numbers_from_file(
                         self.paths.numbers.as_str(),
                         self.message.text.as_str(), 
                        self.numbers_collector.counter
                    );
                }
                ui.separator();
                ui.centered_and_justified(|ui| ui.label((RichText::new("Powered by Freedom Corporation").size(10.0))));
        });
    }
}

struct Paths {
    chatlog: String,
    players: String,
    numbers: String,
    settings: String
}

struct SampCommands;

struct Player {
    id: usize
}

struct Filter;

struct PlayersCollector {
    list: Vec<String>,
    count: String,
    counter: usize
}

struct NumbersCollector {
    list: Vec<String>,
    count: String,
    counter: usize
}

struct Message {
    text: String
}

struct Delay;

struct LangSwitcher;

struct MyFile;

struct RegexTemplates {
    by_lvl: String,
    by_number: String
}

impl Paths {
    fn new() -> Self {
        Self {
            chatlog: String::from("C:\\Users\\NICKNAME\\Documents\\GTA San Andreas User Files\\SAMP\\chatlog.txt"),
            players: String::from("C:\\Users\\NICKNAME\\Documents\\GTA San Andreas User Files\\SAMP\\players.txt"),
            numbers: String::from("C:\\Users\\NICKNAME\\Documents\\GTA San Andreas User Files\\SAMP\\numbers.txt"),
            settings: String::from("sender_settings.txt")
        }
    }
}

impl SampCommands {
    fn print_id(player_id: usize) {
        let keyboard_layout = LangSwitcher::new();
        keyboard_layout.set_english_language();
        let mut enigo = Enigo::new();
        let mut command = String::from("/id ");
        command.push_str(player_id.to_string().as_str());
        enigo.key_click(Key::F6);
        enigo.key_sequence(command.as_str());
        enigo.key_click(Key::Return);
    }

    fn print_number(player_id: usize) {
        let keyboard_layout = LangSwitcher::new();
        keyboard_layout.set_english_language();
        let mut enigo = Enigo::new();
        let mut command = String::from("/number ");
        command.push_str(player_id.to_string().as_str());
        enigo.key_click(Key::F6);
        enigo.key_sequence(command.as_str());
        enigo.key_click(Key::Return);
    }

    fn send_sms(number: u32, text: &str) {
        let keyboard_layout = LangSwitcher::new();
        keyboard_layout.set_russian_language();
        Delay::one_second();
        let mut enigo = Enigo::new();
        let mut command = String::from("/sms ");
        command.push_str(number.to_string().as_str());
        command.push_str(" ");
        command.push_str(text);
        enigo.key_click(Key::F6);
        enigo.key_sequence(command.as_str());
        enigo.key_click(Key::Return);
    }
}

impl Player {
    fn new() -> Self {
        Self {
            id: 0
        }
    }

    fn increment(&mut self) {
        self.id += 1;
    }
}

impl Filter {
    fn by_lvl(regex_lvl_template: String, text_from_chatlog: String) -> String {
        let template = Regex::new(regex_lvl_template.as_str()).unwrap();
        let mut is_target = String::new();
        match template.captures(text_from_chatlog.as_str()) {
            Some(level_from_template) => { 
                is_target = level_from_template[0].to_string(); 
                println!("Target has been detected."); }
            None => { 
                is_target = " ".to_string(); 
                println!("Detect target failed."); }
        }
        return is_target;
    }

    fn by_number(regex_number_template: String, text_from_chatlog: String) -> String {
        let template = Regex::new(regex_number_template.as_str()).unwrap();
        let mut phone_number = String::new();
        match template.captures(text_from_chatlog.as_str()) {
            Some(number_from_template) => phone_number = number_from_template[0].to_string(),
            None => phone_number = " ".to_string()
        }

        if Filter::isNotPlayerNumber(phone_number.as_str()) {
            phone_number = " ".to_string();
        }
        return phone_number;
    }

    fn isNotPlayerNumber(number: &str) -> bool {
        match number {
            "999999" => true,
            _ => false
        }
    }
}

impl PlayersCollector {

    fn new() -> Self {
        Self {
            list: vec![],
            count: String::new(),
            counter: 0
        }
    }
    
    fn print_player(&self, player_id: usize) {
        SampCommands::print_id(player_id);
    }

    fn collect_player(&mut self, chatlog_path: &str) {
        let mut chatlog_text = String::new();
        let mut player = String::new();
        let file = File::open(chatlog_path).unwrap();
        let mut reader = BufReader::new(
        DecodeReaderBytesBuilder::new()
            .encoding(Some(WINDOWS_1251))
            .build(file));
        for line in reader.lines().last() {
            for text in line {
                chatlog_text = text;
                chatlog_text.clone();
                let mut template = RegexTemplates::new();
                template.by_lvl = String::from(r"\[\d{1,3}\]\s\d\sуровень");
                player = Filter::by_lvl(template.by_lvl, chatlog_text);
                if player != " " {
                    player.push_str("\n");
                    self.list.push(player);
                }
                
            }
        }
    }

    fn save_players_in_file(&self, players_path: &str, players_list: Vec<String>) {
        let mut list = String::new();
        list = players_list.into_iter().collect();
        MyFile::write(players_path, list.as_str());
    }

    fn get_players_from_file(&self, players_path: &str, players_count: usize) -> Vec<String> {
        let mut id_list: Vec<String> = vec![];
        let mut flag: bool = false;
        let mut tmp_str_id = String::new();
        let mut id: usize = 0;
        let file = File::open(players_path).unwrap();
        let mut reader = BufReader::new(file);
        if id < players_count {
            for line in reader.lines() {
                println!("{:?}", line);
                for text in line {
                    for char in text.chars() {
                        if flag {
                            if char.is_digit(10) {
                                tmp_str_id.push_str(char.to_string().as_str());
                                id += 1;
                            }
                            else {
                                id_list.push(tmp_str_id.to_string());
                                tmp_str_id.clear();
                                flag = false;
                                break;
                            }
                        }
                        if char == '[' {
                            flag = true;
                            continue;   
                        }
                    }
                }
            }
        }
        else {
            println!("Collect id has been failed");
        }
        
        id_list
        
    }
}

impl NumbersCollector {

    fn new() -> Self {
        Self {
            list: vec![],
            count: String::new(),
            counter: 0
        }
    }

    fn get_number(&self, chatlog_path: &str) -> String {
        Delay::one_second();
        let mut number = String::new();
        let file = File::open(chatlog_path).unwrap();
        let mut reader = BufReader::new(
        DecodeReaderBytesBuilder::new()
            .encoding(Some(WINDOWS_1251))
            .build(file));
        let mut regex_template = RegexTemplates::new();
        regex_template.by_number = String::from(r"\d{6}");
        regex_template.by_number.clone();
        for line in reader.lines().last() {
            for text in line {
                text.clone();
                number = Filter::by_number(regex_template.by_number, text);
                number.clone();
                break;
            }
            break;
        }
            number
    }

    fn get_numbers_count(&self, numbers_path: &str, list: Vec<String>) -> usize {
        let count = list.len();
        count
    }

}

impl Message {
    fn new() -> Self {
        Self {
            text: String::new()
        }
    }

    fn attack_numbers_from_file(&self, numbers_path: &str, text: &str, numbers_count: usize) {
        let keyboard_layout = LangSwitcher::new();
        let mut file = File::open(numbers_path).unwrap();
        let mut reader = BufReader::new(file);
        let mut i: usize = 0;
        for line in reader.lines() {
            if i < numbers_count {
                for number in line {
                    SampCommands::send_sms(number.parse().unwrap(), text);
                    i += 1;
                }
            }
            else {
                break;
            }
        }
    }
}

impl Delay {
    
    fn ten_seconds() {
        let seconds = time::Duration::from_millis(10000);
        sleep(seconds);
    }
    
    fn one_second() {
        let second = time::Duration::from_millis(1000);
        sleep(second);
    }
}

impl LangSwitcher {
    fn new() -> Self {
        Self
    }

    fn set_english_language(&self) {
        unsafe {
            PostMessageW(GetForegroundWindow(), WM_INPUTLANGCHANGEREQUEST, INPUTLANGCHANGE_SYSCHARSET, 0x409);
        }
    }

    fn set_russian_language(&self) {
        unsafe {
            PostMessageW(GetForegroundWindow(), WM_INPUTLANGCHANGEREQUEST, INPUTLANGCHANGE_SYSCHARSET, 0x419);
        }
    }
}

impl MyFile {

    fn read(path: &str) -> String {
        let mut file = File::options().read(true).write(true).open(path).unwrap();
        let mut data = String::new();
        file.read_to_string(&mut data).unwrap();
        data
    }

    fn write(path: &str, data: &str) {
        let mut file = File::create(&path);
        let mut file_new = File::options().write(true).open(&path).unwrap();
        let bytes = file_new.write_all(data.as_bytes()).unwrap();
    }

    fn write_append(path: &str, data: &str) {
        let mut file = File::options()
                                                .write(true)
                                                .append(true)
                                                .open(&path)
                                                .unwrap();
        file.write(data.as_bytes()).unwrap();
    }
}

impl RegexTemplates {
    fn new() -> Self {
        Self {
            by_lvl: String::new(),
            by_number: String::new()
        }
    }

    fn as_str(&self) -> &str {
        &self.as_str()
    }
}